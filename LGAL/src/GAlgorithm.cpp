#include "GAlgorithm.h"
#include <time.h>
#include <algorithm>
#include <iostream>
#include <random>


GAlgorithm::GAlgorithm(int genomeSize, int PopSize, int GenNum, Simulation* fit)
{
	srand(time(NULL));
	m_genomeSize = genomeSize;
	m_maxPopSize = PopSize;
	m_generations = GenNum;
	sim = fit;
}

GAlgorithm::GAlgorithm(int genomeSize, int PopSize, int GenNum, int minGeneValue, int maxGeneValue, Simulation* fit)
{
	srand(time(NULL));
	m_genomeSize = genomeSize;
	m_maxPopSize = PopSize;
	m_generations = GenNum;
	MinGene = minGeneValue;
	MaxGene = maxGeneValue;
	sim = fit;
}

GAlgorithm::~GAlgorithm()
{
}

void GAlgorithm::FreshPopulation()
{
	std::random_device random;
	random.max();
	for (size_t i = 0; i < m_maxPopSize; i++)
	{
		// create a blank organism
		std::vector<int> organism;
		for (size_t i = 0; i < m_genomeSize; i++)
		{	// randomise the directions in it
			organism.push_back((rand() % MaxGene + MinGene));
		}
		population.push_back({ organism, 0 });
	}
}

void GAlgorithm::Breed(const std::vector<organism> &prevGeneration)
{
	// clear current population
	population.clear();

	// possible breeding strategies:
	// 1. Breed pairs with a split point
	// 2. Breed pairs with multiple splits
	// 3. Breed in more than pairs

	for (size_t i = 0; i < prevGeneration.size(); i++)
	{
		// create two pairings for every organism
		int j = rand() % prevGeneration.size();
		while (i == j)
		{	// make sure that i and j aren't the same
			j = rand() % prevGeneration.size();
		}
		int k = rand() % prevGeneration.size();
		while (i == k || j == k)
		{	// make sure that the pairing isn't the same as previous
			k = rand() % prevGeneration.size();
		}

		// Pairs with a single split point
		int split = rand() % m_genomeSize;
		organism newborn;
		newborn.fitness = 0;
		size_t n = 0;
		//for (; n <= split; n++)
		//{
		//	newborn.genome.push_back(prevGeneration[i].genome[n]);
		//}
		for (; n < m_genomeSize; n++)
		{
			newborn.genome.push_back(prevGeneration[j].genome[n]);
		}
		// mutate here
		Mutate(newborn);
		population.push_back(newborn);

		split = rand() % m_genomeSize;
		organism newborn2;
		newborn2.fitness = 0;
		n = 0;
		//for (; n <= split; n++)
		//{
		//	newborn2.genome.push_back(prevGeneration[i].genome[n]);
		//}
		for (; n < m_genomeSize; n++)
		{
			newborn2.genome.push_back(prevGeneration[k].genome[n]);
		}
		Mutate(newborn2);
		population.push_back(newborn2);
	}

}

void GAlgorithm::Mutate(organism& genome)
{
	int t = rand() % 100;
	if (t < 10)
	{
		int m = rand() % m_genomeSize;
		int d = (int)(rand() % 10 + 1);
		genome.genome[m] = d;
	}
}

void GAlgorithm::Train()
{
	for (int g = 0; g < m_generations; g++)
	{
		for (int i = 0; i < m_maxPopSize; i++)
		{
			population[i].fitness = sim->Fitness(population[i].genome);
		}

		std::sort(population.begin(), population.end());

		Best = population[0];

		sim->GenerationReport(Best.genome, Best.fitness, g);

		population.erase(population.begin() + population.size() / 2, population.end());

		std::vector<organism> oldPop = population;
		Breed(oldPop);
	}
}

int Simulation::Fitness(std::vector<int>)
{
	return 0;
}

void Simulation::GenerationReport(std::vector<int>, int fitness, int generation)
{
	std::cout << "Undefined Report" << std::endl;
}
