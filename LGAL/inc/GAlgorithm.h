#pragma once
#include <vector>
//template <typename T>
class Simulation
{
public:
	virtual int Fitness(std::vector<int>);
	virtual void GenerationReport(std::vector<int>, int fitness, int generation);
};
class GAlgorithm
{
public:
	struct organism {
		std::vector<int> genome;
		int fitness;

		bool operator<(const organism& a) const
		{
			return fitness < a.fitness;
		}
	};

	GAlgorithm(int genomeSize, int PopSize, int GenNum, Simulation* fit);
	GAlgorithm(int genomeSize, int PopSize, int GenNum, int minGeneValue, int maxGeneValue, Simulation* fit);
	~GAlgorithm();

	void FreshPopulation();
	void Breed(const std::vector<organism> &prevGeneration);
	void Mutate(organism& genome);
	void Train();
private:

	int m_genomeSize;
	int m_maxPopSize;
	int m_generations;
	std::vector<organism> population;
	std::vector<int> currentGenome;

	int MinGene;
	int MaxGene;

	organism Best;
	Simulation* sim;
};

